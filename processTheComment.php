
<!DOCTYPE html>
<html>
<head>
    <title>Komentarz przyjęty</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body class="container">
<?php
//UTWORZENIE KRÓTKICH NAZW ZMIENNYCH
$name = trim($_POST['name']);  //trim usuwa znaki odstępu z początku i końca łańcucha
$email = trim($_POST['email']);
$comment = trim($_POST['comment']);

//ZDEFINIOWANIE DANYCH STATYCZNYCH
$addressTo = "praski.martin@gmail.com";
$topic = "Komentarz ze strony WWW";
$content = "Nazwa klienta: ".$name."\n".
    "Adres pocztowy: ".$email."\n".
    "Komentarz klienta: \n".str_replace("\r\n","",$comment)."\n"; //czysci tekst - zamienia \r\n na ""

mail($addressTo, $topic, $content);
?>
<h1>Komentarz przyjęty</h1>
<p>Państwa komentarz (przedstawiony poniżej) został wysłany.</p>
<!--// nl2br zamienia znaki nowej linii jak \n na znak HTML-->
<!--nowej linii <br/>-->
<p><?php echo nl2br(htmlspecialchars($content)); ?></p>
</body>
</html>
